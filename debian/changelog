libtest-autoloader-perl (0.03-5) unstable; urgency=medium

  [ gregor herrmann ]
  * Change bugtracker URL(s) to HTTPS.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 17 Jun 2022 11:51:32 +0100

libtest-autoloader-perl (0.03-4.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 14 Jun 2022 14:41:28 +0200

libtest-autoloader-perl (0.03-4) unstable; urgency=medium

  * Team upload.

  [ Lucas Kanashiro ]
  * Bump debhelper compatibility level to 9
  * Declare compliance with Debian policy 3.9.6

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * Change order of alternative (build) dependencies.
  * Declare compliance with Debian Policy 3.9.7.

 -- gregor herrmann <gregoa@debian.org>  Sun, 21 Feb 2016 20:22:10 +0100

libtest-autoloader-perl (0.03-3) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Strip trailing slash from metacpan URLs.
  * Update Test::Tester build dependency.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Set the locale to C for the test suite. (Closes: #807357)
  * Add myself to Uploaders.
  * Make the package autopkgtestable.

 -- Niko Tyni <ntyni@debian.org>  Mon, 07 Dec 2015 23:08:27 +0200

libtest-autoloader-perl (0.03-2) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Add patch from CPAN RT to fix build failure with Perl >= 5.16.
    (Closes: #711259)
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Bump debhelper compatibility level to 8.
  * Set Standards-Version to 3.9.4 (no further changes).

 -- gregor herrmann <gregoa@debian.org>  Sun, 16 Jun 2013 17:20:25 +0200

libtest-autoloader-perl (0.03-1) unstable; urgency=low

  * Initial Release. (Closes: #616490)

 -- Nicholas Bamber <nicholas@periapt.co.uk>  Sat, 05 Mar 2011 10:29:50 +0000
